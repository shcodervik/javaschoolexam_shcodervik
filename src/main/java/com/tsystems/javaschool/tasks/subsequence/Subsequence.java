package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        boolean result = false;
        if(checkArguments(x,y)){
            for (Object xItem : x) {
                result = false;
                Iterator yIterator = y.iterator();

                while (yIterator.hasNext()) {
                    if (myEquals(yIterator.next(), xItem)) {
                        result = true;
                        yIterator.remove();
                        break;
                    }
                    yIterator.remove();
                }

                if (!result) return result;
            }
        }
        else{
            if(x.isEmpty()) result = true;
        }
        return result;
    }


    private boolean myEquals(Object a, Object b)
    {
        if (a == null && b == null)
            return true;
        else if (a != null && b != null)
            return a.equals(b);
        return false;
    }

    private boolean checkArguments(List a, List b){
        if(a == null || b == null){
            throw new IllegalArgumentException();
        }
        return !a.isEmpty() && !b.isEmpty();
    }

}
