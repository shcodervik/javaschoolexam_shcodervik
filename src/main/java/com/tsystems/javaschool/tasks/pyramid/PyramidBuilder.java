package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */


    private final int step = 2;
    private int rowCount = 0;
    private int colCount = 0;




    private boolean CheckArray(int length)
    {
        double n = (-1 + Math.sqrt(1 + 8 * length)) / 2; // return count of rows
        int tmp = (int) n;
        if (tmp == n){ // building matrix is possible
            rowCount = tmp;
            colCount = 2 * rowCount - 1;
            return true;
        }
        else throw new CannotBuildPyramidException("Building pyramid is impossible!");
    }



    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        if (inputNumbers == null) throw new CannotBuildPyramidException("Array is null!");
        CheckArray(inputNumbers.size());
        int[][] result = new int[rowCount][colCount];

        int l = inputNumbers.size() - 1;
        try {
            Collections.sort(inputNumbers);
        }catch(NullPointerException npe){
            throw new CannotBuildPyramidException(npe.getMessage());
        }
        for(int i = 0; i < rowCount; i++) {
            for (int j = (colCount - 1) - i; j >= i; j -= step) {
                result[rowCount - 1 - i][j] = inputNumbers.get(l--);
            }
        }
        return result;
    }




}
