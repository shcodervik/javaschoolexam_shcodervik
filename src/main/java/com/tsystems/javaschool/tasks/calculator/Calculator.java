package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private char decimator = '.';
    private Set<String> operators = new HashSet<>(Arrays.asList("+", "-", "*","/", "(", ")"));

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if((statement == null)||(statement.isEmpty()))
            return null;
        List<String> separated;
        try {
            separated = separate(statement);
        } catch (Exception e) {
            return null;
        }
        List<String> rpnExpression;
        try {
            rpnExpression = toRPN(separated);
        } catch (Exception e) {
            return null;
        }
        Stack<String> stack = new Stack<>();
        ArrayDeque<String> queue = new ArrayDeque<String>(rpnExpression);
        String str = queue.poll();
        while (queue.size() >= 0){
            if (!operators.contains(str)){
                stack.push(str);
                str = queue.poll();
            }else{
                double summ = 0;
                try{
                    switch (str) {
                        case "+": {
                            double a = Double.parseDouble(stack.pop());
                            double b = Double.parseDouble(stack.pop());
                            summ = a + b;
                            break;
                        }
                        case "-": {
                            double a = Double.parseDouble(stack.pop());
                            double b = Double.parseDouble(stack.pop());
                            summ = b - a;
                            break;
                        }
                        case "*": {
                            double a = Double.parseDouble(stack.pop());
                            double b = Double.parseDouble(stack.pop());
                            summ = b * a;
                            break;
                        }
                        case "/": {
                            double a = Double.parseDouble(stack.pop());
                            if(a == 0)
                                return null;
                            double b = Double.parseDouble(stack.pop());
                            summ = b / a;
                            break;
                        }
                    }
                }catch (Exception ex){
                    return null;
                }
                stack.push(String.valueOf(summ));
                if (queue.size() > 0)
                    str = queue.poll();
                else
                    break;
            }

        }
        return valueOf(round(Double.parseDouble(stack.pop()),4));
    }

    private List<String> toRPN(List<String> separatedExpression) throws Exception {
        List<String> outputSeparated = new ArrayList<String>();
        Stack<String> stack = new Stack<>();
        for (String c : separatedExpression){
            if (operators.contains(c)){
                if (stack.size()> 0 && !c.equals("(")){
                    if (c.equals(")")){
                        String s = stack.pop();
                        while (!s.equals("(")){
                            outputSeparated.add(s);
                            s = stack.pop();
                        }
                    }
                    else if (getPriority(c) > getPriority(stack.peek()))
                        stack.push(c);
                    else{
                        while (stack.size() > 0 && getPriority(c) <= getPriority(stack.peek()))
                            outputSeparated.add(stack.pop());
                        stack.push(c);
                    }
                }
                else
                    stack.push(c);
            }
            else
                outputSeparated.add(c);
        }
        if (stack.size() > 0)
            while(!stack.empty())
                outputSeparated.add(stack.pop());
        if (outputSeparated.contains("(") || outputSeparated.contains(")"))
            throw new Exception("incorrect input string");
        return outputSeparated;
    }

    private List<String> separate(String input) throws Exception {
        int pos = 0;
        ArrayList<String> result = new ArrayList<String>();

        boolean isNegotiation = true;
        while (pos < input.length()){
            StringBuilder s = new StringBuilder();
            s.append(input.charAt(pos));
            if (!operators.contains(""+input.charAt(pos)) || (isNegotiation && (input.charAt(pos) == '-'))){
                if (Character.isDigit(input.charAt(pos)) || input.charAt(pos) == '-')
                    for (int i = pos + 1; i < input.length() &&
                            (Character.isDigit(input.charAt(i)) || (input.charAt(i) == decimator && s.charAt(s.length()-1)!=decimator)); i++)
                        s.append(input.charAt(i));
                else throw new Exception("incorrect input string");
            }
            result.add(s.toString());
            isNegotiation = result.get(result.size()-1).equals("(");
            pos += result.get(result.size()-1).length();
        }
        return result;
    }

    private byte getPriority(String s) {
        switch (s) {
            case "(":
            case ")":
                return 0;
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
                return 2;
            default:
                return 3;
        }
    }


    private double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        if (value == Math.round(value))
            return Math.round(value);
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    private String valueOf(double value) {
        String result = String.valueOf(value);
        String[] splitted = result.split("[.]");
        if (splitted[splitted.length-1].replaceAll("0", "").equals(""))
            return splitted[0];
        else return result;
    }

}
